//
//  AuthManager.h
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthServiceManager : NSObject

- (void)login:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

@end

