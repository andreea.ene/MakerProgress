//
//  ActivityStatus.m
//  MakerProgress
//
//  Created by Marius Stratulat on 11/04/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//
#import <Foundation/Foundation.h>

/*
extern const struct ActivityStatusEnum
{
    __unsafe_unretained NSString *Completed;
    __unsafe_unretained NSString *Uncompleted;
} ActivityStatus;

const struct ActivityStatusEnum ActivityStatus =
{
    .Completed = @"completed",
    .Uncompleted = @"uncompleted"
};
 
 */
