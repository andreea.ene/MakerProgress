//
//  AddActivityViewController.m
//  MakerProgress
//
//  Created by David on 04/04/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "AddActivityViewController.h"
#import "ActivitiesServiceManager.h"
#import "TodayActivitiesViewController.h"
#import "ActivitiesModel.h"

@interface AddActivityViewController ()

@property (strong, atomic) ActivitiesServiceManager* activitiesServiceManager;

@end

@implementation AddActivityViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.activitiesServiceManager = [[ActivitiesServiceManager alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendButtonTouch:(id)sender {
    //get local time
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    NSDate *startDateInLocalTimezone = [self.startDateDatepicker.date dateByAddingTimeInterval:timeZoneSeconds];
    NSTimeInterval startDate = [startDateInLocalTimezone timeIntervalSince1970];
    NSDate *endDateInLocalTimezone = [self.endDateDatepicker.date dateByAddingTimeInterval:timeZoneSeconds];
    NSTimeInterval endDate = [endDateInLocalTimezone timeIntervalSince1970];

    NSString *status = @"uncompleted";
    
    NSDictionary *params = @{@"name": self.nameTextField.text,
                             @"body": self.descriptionTextField.text,
                             @"start_date": @"1488482011",
                             @"end_date": @"1488492011",
                             @"status": status};
    
    [self.activitiesServiceManager create:params
                                  handler:^(id responseObject){
                                      NSLog(@"Activity created: %@", responseObject);
                                    
                                      TodayActivitiesViewController *instanceTodayActivitiesVC = [[TodayActivitiesViewController alloc] init];
                                      self.delegate = instanceTodayActivitiesVC;
                                      
                                      if ([self.delegate respondsToSelector:@selector(dashboardViewControllerStartLoad:)]){
                                          
                                          NSError *errorDictorionary;
                                          ActivitiesModel *todayActivities = [[ActivitiesModel alloc] initWithDictionary:responseObject error:&errorDictorionary];
                                          
                                          if(errorDictorionary){
                                              NSLog(@"Error dictionary: %@", errorDictorionary);
                                          }
                                          
                                          [self.delegate dashboardViewControllerStartLoad:todayActivities];
                                      }
                                      
                                      //maybe send new data to previous controller or recall api
                                      [self.navigationController popViewControllerAnimated:YES];
                                  }
                                  errorHandler:^(NSError *error){
                                    NSLog(@"Error create activity: %@", error);
                                  }];
}

@end
