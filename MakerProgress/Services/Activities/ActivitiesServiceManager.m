//
//  AuthManager.m
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "ActivitiesServiceManager.h"
#import "BaseServiceManager.h"

@implementation ActivitiesServiceManager

- (void)getToday:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler {
    NSString *apiUrl = @"activities";
    [BaseServiceManager.sharedManager GET:apiUrl params:nil handler:handler errorHandler:errorHandler];
}

- (void)getByPeriod:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler{
    NSString *apiUrl = @"activities";
    [BaseServiceManager.sharedManager GET:apiUrl params:params handler:handler errorHandler:errorHandler];
}

- (void)getById:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler {
    NSString *apiUrl = @"activities/:id";
    [BaseServiceManager.sharedManager POST:apiUrl params:params handler:handler errorHandler:errorHandler];
}

- (void)create:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler {
    NSString *apiUrl = @"activities";
    [BaseServiceManager.sharedManager POST:apiUrl params:params handler:handler errorHandler:errorHandler];
}

- (void)update:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler {
    NSString *apiUrl = @"activities/:id";
    [BaseServiceManager.sharedManager PUT:apiUrl params:params handler:handler errorHandler:errorHandler];
}

- (void)delete:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler {
    NSString *apiUrl = @"activities/:id";
    [BaseServiceManager.sharedManager DELETE:apiUrl params:params handler:handler errorHandler:errorHandler];
}

@end

