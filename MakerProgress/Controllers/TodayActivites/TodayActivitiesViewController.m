//
//  TodayActivitiesViewController.m
//  MakerProgress
//
//  Created by David on 09/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

//to do: custom cell cu tick de done

#import "TodayActivitiesCustomCell.h"
#import "TodayActivitiesViewController.h"

#import "ActivitiesModel.h"
#import "ActivitiesServiceManager.h"

@interface TodayActivitiesViewController ()

@property (strong, nonatomic) NSMutableArray *tableData;

@property (strong, atomic) ActivitiesServiceManager* activitiesServiceManager;

@end

@implementation TodayActivitiesViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.activitiesServiceManager = [[ActivitiesServiceManager alloc] init];
    
    [self.activitiesServiceManager getToday:^(id responseObject){
            self.tableData = responseObject;
            [self.tableView reloadData];
        }
        errorHandler:^(NSError *error){
            NSLog(@"Error: %@", error);
        }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *tableIdentifier = @"tableItem";
    
    TodayActivitiesCustomCell *cell = (TodayActivitiesCustomCell *)[tableView dequeueReusableCellWithIdentifier:tableIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TodayActivitiesTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSError *error;
    ActivitiesModel *todayActivities = [[ActivitiesModel alloc] initWithDictionary:self.tableData[indexPath.row] error:&error];
    
    //cell.id = [NSString stringWithString:todayActivities.id];
    cell.item = todayActivities;
    cell.nameLabel.text = [NSString stringWithString:todayActivities.name];
    cell.descriptionLabel.text = [NSString stringWithString:todayActivities.body];
    cell.timeDurationLabel.text = [NSString stringWithString:[todayActivities timeFormatted]];
    cell.imageView.image = [UIImage imageNamed:@"calendar"];
    
    if ([todayActivities.status  isEqual: @"uncompleted"]){
        cell.checkBox.on = NO;
    }
    else{
        //posibil ceva stare intermediara
        cell.checkBox.on = YES;
    }
    
    //[self.checkboxArray arrayByAddingObject:cell.checkBox]; might not need this
    
    // ++ api call atunci cand marchezi checkbox drept complete
    // navigation controller pt next day and previous day
   // NSLog(@"%@", error);
    
    return cell;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tableData.count;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToMainStoryboard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"DashboardViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
}

- (void)dashboardViewControllerStartLoad: (ActivitiesModel *)newActivity {
    [self.tableData addObject:newActivity];
    [self.tableView reloadData];
    
    NSLog(@"dashBoardViewControllerStartLoad");

}

- (IBAction)previousDayButtonTouch:(id)sender {
}
@end
