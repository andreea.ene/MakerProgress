//
//  AddActivityViewController.h
//  MakerProgress
//
//  Created by David on 04/04/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivitiesModel.h"

@protocol TodayActivitiesViewControllerDelegate <NSObject>

- (void) dashboardViewControllerStartLoad:(ActivitiesModel *)newActivity;

@end

@interface AddActivityViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *startDateDatepicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *endDateDatepicker;
@property (weak, nonatomic) IBOutlet UITextField *hourTextField;
@property (weak, nonatomic) IBOutlet UITextField *minutesTextField;

@property (weak, nonatomic) id delegate;

@end
