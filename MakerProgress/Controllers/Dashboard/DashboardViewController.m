//
//  DashboardViewController.m
//  MakerProgress
//
//  Created by Andreea-Daniela Ene on 07/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "DashboardViewController.h"
#import "ActivitiesModel.h"
#import "ActivitiesServiceManager.h"

#import "DashboardCustomCell.h"
#import "ActivitiesModel.h"

@interface DashboardViewController (){
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    
    NSDate *_dateSelected;
    
}

@property (strong, nonatomic) __block NSMutableArray *calendarData;
@property (strong, nonatomic) NSArray *dailyEvents;

@property (strong, atomic) ActivitiesServiceManager* activitiesServiceManager;

@end


@implementation DashboardViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(!self){
        return nil;
    }
    
    self.title = @"Dashboard";
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self getDataFromAPI];
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    self.activitiesServiceManager = [[ActivitiesServiceManager alloc] init];
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:_todayDate];
    
    [self getDataFromAPI];
}

#pragma mark - Buttons callback
- (IBAction)didGoTodayTouch
{
    [_calendarManager setDate:_todayDate];
}

- (IBAction)goToTodayActivitiesStoryboard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TodayActivities" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"TodayActivitiesNavigationController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
}

- (IBAction)didChangeModeTouch
{
    _calendarManager.settings.weekModeEnabled = !_calendarManager.settings.weekModeEnabled;
    [_calendarManager reload];
    //
    //	dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    //	[UIView transitionWithView:dayView
    //					  duration:.3
    //					  options:0
    //                    animations:^{
    //						dayView.circleView.transform = CGAffineTransformIdentity;
    //						[_calendarManager reload];
    //                    } completion:nil];
    CGFloat newHeight;
    if (self.calendarContentViewHeight.constant == 300) {
        newHeight = 80;
        [UIView animateWithDuration:0.4 animations:^{
            if(_calendarManager.settings.weekModeEnabled){
                self.calendarContentViewHeight.constant = newHeight;
                [self.view layoutIfNeeded];
            }
        }];
    }
    else {
        newHeight = 300;
        [UIView animateWithDuration:0.4 animations:^{
            if(!_calendarManager.settings.weekModeEnabled){
                self.calendarContentViewHeight.constant = newHeight;
                [self.view layoutIfNeeded];
            }
        }];
    }
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    } // doesn t seem like we need this for now !!
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
    
    NSString *key = [[self dateFormatter] stringFromDate:_dateSelected];
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        
        NSMutableArray *events =_eventsByDate[key];
        self.dailyEvents = events;
        
        for (ActivitiesModel* item in events ) {
            NSLog(@"Press: %@", item);
        }
        
        [self.tableView reloadData];
        self.tableView.hidden = NO;
    }
    else{
        self.tableView.hidden = YES;
    }
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //NSLog(@"Previous page loaded");
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 6 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-6];
    
    // Max date will be 6 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months: 6];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    //ideea e ca intra aici inainte ca _eventsByDate sa fie initializat cu datele pe care le vrem
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
}

- (void)addEventsToCalendar
{   //eventual sa primeasca table data parametru si sa il transforme in events by date;
    
    _eventsByDate = [NSMutableDictionary new];
    
    for (ActivitiesModel *item in self.calendarData) {
        NSString *key = [[self dateFormatter] stringFromDate:[item dateFormatted]];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:item];
    }
}

- (void) getDataFromAPI {
    
    NSDictionary *params = @{};
    
    [self.activitiesServiceManager getByPeriod:params handler:^(id response) {
        self.calendarData = [NSMutableArray new];
        
        ActivitiesModel *todayActivities = [[ActivitiesModel alloc] init];
        
        for (NSDictionary* responseItem in response) {
            NSError *errorDictorionary;
            todayActivities = [[ActivitiesModel alloc] initWithDictionary:responseItem error:&errorDictorionary];
            
            if(errorDictorionary){
                NSLog(@"Error dictionary: %@", errorDictorionary);
            }
            
            [self.calendarData addObject:todayActivities];
        }
        
        [self addEventsToCalendar];
    } errorHandler:^(NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

#pragma mark TableView


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *tableIdentifier = @"tableItem";
    
    DashboardCustomCell *cell = (DashboardCustomCell *)[tableView dequeueReusableCellWithIdentifier:tableIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DashboardViewTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (self.dailyEvents){
        ActivitiesModel *dailyActivity = self.dailyEvents[indexPath.row];
        if (dailyActivity){
            cell.nameLabel.text = [NSString stringWithString:dailyActivity.name];
            cell.durationLabel.text = [NSString stringWithString:dailyActivity.timeFormatted];
        }
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dailyEvents count];
}


@end
