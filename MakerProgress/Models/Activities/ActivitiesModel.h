//
//  ActivitiesModel.h
//  MakerProgress
//
//  Created by David on 16/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ActivitiesModel : JSONModel

@property (nonatomic) NSString *id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *body;
@property (nonatomic) NSInteger startDate;
@property (nonatomic) NSInteger endDate;
@property (nonatomic) NSString *status;

- (NSString *)timeFormatted;
- (NSDate *)dateFormatted;

@end
