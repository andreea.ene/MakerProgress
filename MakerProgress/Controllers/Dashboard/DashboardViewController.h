//
//  DashboardViewController.h
//  MakerProgress
//
//  Created by Andreea-Daniela Ene on 07/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>


@interface DashboardViewController : UIViewController<JTCalendarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarContentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
