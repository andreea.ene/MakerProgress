//
//  TodayActivitiesViewController.h
//  MakerProgress
//
//  Created by David on 09/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BEMCheckBox.h"
#import "AddActivityViewController.h"

@interface TodayActivitiesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, TodayActivitiesViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addNewActivityButton;
- (IBAction)nextDayButtonTouch:(id)sender;
- (IBAction)previousDayButtonTouch:(id)sender;


@end
