//
//  BaseManager.m
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "BaseServiceManager.h"

@interface BaseServiceManager()

@property (strong, nonatomic) AFHTTPSessionManager *_sessionManager;
@property (strong, nonatomic) NSURL* _baseUrl;

- (AFHTTPSessionManager *) createSessionManager;

@end

@implementation BaseServiceManager

+ (id)sharedManager {
    static BaseServiceManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        //move to user settings
        NSString *baseUrlString =@"http://private-anon-4b3acaaf41-makerprogress.apiary-mock.com";
        self._baseUrl = [NSURL URLWithString:[baseUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        
        self._sessionManager = [self createSessionManager];
    }
    return self;
}

-(void)GET:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {
    
    [self._sessionManager GET:api parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        handler (responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        errorHandler (error);
    }];
}

-(void)POST:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {

    [self._sessionManager POST:api parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        handler (responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        errorHandler (error);
    }];
}

-(void)PUT:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {

    [self._sessionManager PUT:api parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        handler (responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        errorHandler (error);
    }];
}

-(void)DELETE:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {

    [self._sessionManager DELETE:api parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        handler (responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        errorHandler (error);
    }];
}

-(void)PATCH:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {

    [self._sessionManager PATCH:api parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        handler (responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        errorHandler (error);
    }];
}

-(void)setAuthorizationToken:(NSString *)_newAuthorizationToken{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_newAuthorizationToken forKey:@"authorizationToken"];
    [self._sessionManager.requestSerializer setValue:_newAuthorizationToken forHTTPHeaderField:@"Authorization"];
}

-(void)clearAuthorizationToken {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"authorizationToken"];
    [self._sessionManager.requestSerializer clearAuthorizationHeader];
}

- (AFHTTPSessionManager *) createSessionManager {
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *newSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:self._baseUrl sessionConfiguration:config];
    
    //newSessionManager.securityPolicy.allowInvalidCertificates =YES;

    newSessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    newSessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return newSessionManager;
}

@end

