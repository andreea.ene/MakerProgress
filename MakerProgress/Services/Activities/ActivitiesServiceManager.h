//
//  AuthManager.h
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivitiesServiceManager: NSObject

- (void)create:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

- (void)update:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

- (void)delete:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

- (void)getToday:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

- (void)getByPeriod:(NSDictionary *)params handler:(void (^)(id response))handler errorHandler:(void (^)(NSError *error))errorHandler;

@end

