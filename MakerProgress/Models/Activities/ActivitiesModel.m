//
//  ActivitiesModel.m
//  MakerProgress
//
//  Created by David on 16/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "ActivitiesModel.h"

@implementation ActivitiesModel
+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"startDate": @"start_date",
                                                                  @"endDate": @"end_date",
                                                                  @"id": @"id"
                                                                  }];
}

- (NSString *)timeFormatted {
    NSDate* startDate = [NSDate dateWithTimeIntervalSince1970:self.startDate];
    NSDate* endDate = [NSDate dateWithTimeIntervalSince1970:self.endDate];

    int totalSeconds = (int)[endDate timeIntervalSinceDate:startDate];
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    NSInteger hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02ld:%02d:%02d",(long)hours, minutes, seconds];
}

- (NSDate *)dateFormatted {
	NSDate *startTime = [NSDate dateWithTimeIntervalSince1970:self.startDate];
	return startTime;
}

@end
