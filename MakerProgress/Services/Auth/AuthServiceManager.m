//
//  AuthManager.m
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "AuthServiceManager.h"
#import "BaseServiceManager.h"

@implementation AuthServiceManager

-(void)login:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler {
    
    NSString *apiUrl = @"auth/mobile";
    [BaseServiceManager.sharedManager POST:apiUrl params:params handler:handler errorHandler:errorHandler];
}

@end

