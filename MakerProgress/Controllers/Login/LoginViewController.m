//
//  ViewController.m
//  MakerProgress
//
//  Created by Andreea-Daniela Ene on 23/02/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//	use synx
//				/Users/andreea/Documents/iOS/DevAcademy/Project/MakerProgress/MakerProgress.xcodeproj
//	to update
//

#import "LoginViewController.h"
#import "AuthServiceManager.h"
#import <TwitterKit/TwitterKit.h>
#import "BaseServiceManager.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *signInPlaceholder;

@property (strong, atomic) AuthServiceManager* authManager;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.authManager = [[AuthServiceManager alloc] init];
}

-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self twitterSignUp];
}

- (IBAction)twitterSignIn:(id)sender {
    NSLog(@"twitterSignIn button pressed");
    
    [self twitterLogin];
}

- (void)twitterSignUp {
   
    TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSString *message = [NSString stringWithFormat:@"@%@ logged in! (%@)",
                                 [session userName], [session userID]];
            NSLog(@"%@", message);
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[session authToken] forKey:@"access_token"];
            [userDefaults setObject:[session authTokenSecret] forKey:@"access_token_secret"];

            [self twitterLogin];
        } else {
            NSLog(@"Login error: %@", [error localizedDescription]);
        }
    }];
    
    // TODO: Change where the log in button is positioned in your view
    //	logInButton.center = self.view.center;
	[logInButton setFrame:_signInPlaceholder.frame];
    [self.view addSubview:logInButton];
}

- (void)twitterLogin{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *authorizationToken =[userDefaults valueForKey:@"access_token"];
    if(authorizationToken){
        [params setObject:authorizationToken forKey:@"access_token"];
    }
    NSString *accessTokenSecret =[userDefaults valueForKey:@"access_token_secret"];
    if(accessTokenSecret){
        [params setObject:accessTokenSecret forKey:@"access_token_secret"];
    }
    
    [self.authManager login:params
                    handler:^(id responseObject){
                        NSString *authorizationToken = [responseObject valueForKeyPath:@"authentication_token"];
                        NSString *username = [responseObject valueForKeyPath:@"name"];
                        
                        [BaseServiceManager.sharedManager setAuthorizationToken:authorizationToken];
                        
                        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
                        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                        [self presentViewController:vc animated:YES completion:NULL];
                        
                        NSLog(@"token: %@, username: %@", authorizationToken, username);
                    }
               errorHandler:^(NSError *error){
                   NSLog(@"Error: %@", error);
               }];
}
@end
