//
//  TodayActivitiesCustomCell.m
//  MakerProgress
//
//  Created by David on 23/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import "TodayActivitiesCustomCell.h"
#import "ActivitiesServiceManager.h"

@interface TodayActivitiesCustomCell ()

@property (strong, atomic) ActivitiesServiceManager* activitiesServiceManager;

@end

@implementation TodayActivitiesCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.activitiesServiceManager = [[ActivitiesServiceManager alloc] init];
    
    self.checkBox.animationDuration = 0.1;
    self.checkBox = [[BEMCheckBox alloc] initWithFrame:CGRectMake(330, 20, 40, 40)];
    [self addSubview:self.checkBox];
    self.checkBox.delegate = self;
    // Initialization code
}

- (void)didTapCheckBox:(BEMCheckBox *)checkBox{
    NSString *status = @"uncompleted";

    if(checkBox.on == YES){
        status = @"completed";
    }

    NSDictionary *params = @{@"id": self.item.id,
                             @"name": self.item.name,
                             @"body": self.item.body,
                             @"start_date": [@(self.item.startDate) stringValue],
                             @"end_date": [@(self.item.endDate) stringValue],
                             @"status": status};
    
    [self.activitiesServiceManager update:params
                    handler:^(id responseObject){
                        
                        NSLog(@"token: %@", responseObject);
                        
                        //maybe update the status
                    }
                    errorHandler:^(NSError *error){
                        NSLog(@"Error: %@", error);
                    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
