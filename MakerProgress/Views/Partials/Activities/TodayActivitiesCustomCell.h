//
//  TodayActivitiesCustomCell.h
//  MakerProgress
//
//  Created by David on 23/03/2017.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"
#import "ActivitiesModel.h"

@interface TodayActivitiesCustomCell : UITableViewCell <BEMCheckBoxDelegate>

@property (strong, nonatomic) ActivitiesModel *item;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeDurationLabel;
@property (strong, nonatomic) IBOutlet BEMCheckBox *checkBox;

@end
