//
//  BaseManager.h
//  MakerProgress
//
//  Created by Marius Stratulat on 09/03/17.
//  Copyright © 2017 Andreea-Daniela Ene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface BaseServiceManager : NSObject

+(id)sharedManager;

-(void)GET:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler;

-(void)POST:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler;

-(void)PUT:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler;

-(void)DELETE:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler;

-(void)PATCH:(NSString *)api params:(NSDictionary *)params handler:(void (^)(id))handler errorHandler:(void (^)(NSError *))errorHandler;

// add user singleton to use auth token

-(void)setAuthorizationToken:(NSString *)_newAuthorizationToken;

-(void)clearAuthorizationToken;

@end
